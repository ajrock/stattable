function power = myPowerCalc(d,n1,n2,alpha)
% This is a simplified power calculator based on the results from G*Power.
% First version only processes a non central, normal approximation to the
% Mann-Whitney U test. 
% d = effect size (cohen's d)
% n1/n2 are sample group sizes
% alpha is, well, alpha

if nargin == 3
    alpha = 0.05;
end

k = 3/pi; % Asymptotic relative efficiency vs t test

N = n1 + n2; % total sample size

df = (N - 2)*k; % Degrees of freedom

dnc = d * sqrt((n1*n2*k)/(N)); % non centrality parameter

tCritical =tinv(1-alpha,df); % critical t value

power = 1-cdf('Noncentral t',tCritical,df,dnc);