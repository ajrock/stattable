function [t] = makeMultiStatTable(statVar,statSS1,statSS2,varargin)
% (statVar,statSS1,statSS2,varargin)
% statVar = vector of numerical data
% statSSS = vector of group ids (same length as statVar)
% tit = figure title string
% makeFig = flag for figure output
% HINT: If you made a boxplot or bar graph for thist data previously, use
% your data variable for the statVar and grouping var for statSSS
% makeFig = flag for figure output
% tit = title for figure

% 2019-05-20 AC: added sig star mode
% 2019-05-21 AC: forked from makeStatTable to add possibility of single
% call examining 2D interactions. Acts as a wrapper for the regular
% makeStatTable and simply passes subarguments. Creates a stat table for
% each sensible combination, i.e. holding one condition stable and checking
% variations across other condition

% Force row vectors
if size(statVar,1)==1
    statVar = statVar';
end
if size(statSS1,1)==1
    statSS1 = statSS1';
end
if size(statSS2,1)==1
    statSS2 = statSS2';
end

groupSS1 = unique(statSS1);
groupSS2 = unique(statSS2);
t = table();
varargin{end+1} = 'rowAppend';
varargin{end+1} = '';
for ii = 1:length(groupSS2)
    if iscell(groupSS2)
        ind = contains(statSS2,groupSS2{ii});
    else
        ind = statSS2 == groupSS2(ii);
        varargin{end} = [' ' num2str(groupSS2(ii))];
    end
    varargin{end} = [' ' groupSS2{ii}];
    t = [t; makeStatTable(statVar(ind),statSS1(ind),varargin{:})];
end

for ii = 1:length(groupSS1)
    if iscell(groupSS1)
        ind = contains(statSS1,groupSS1{ii});
        varargin{end} = [' ' groupSS1{ii}];
    else
        ind = statSS1 == groupSS1(ii);
        varargin{end} = [' ' num2str(groupSS1(ii))];
    end
    t = [t; makeStatTable(statVar(ind),statSS2(ind),varargin{:})];
end


