function [d] = myCohensD(x,y)
%myCohensD Calculates Cohen's D between two vectors of numbers
%   Detailed explanation goes here

u1 = mean(x,'omitnan');
u2 = mean(y,'omitnan');

s1 = std(x,'omitnan');
s2 = std(y,'omitnan');

n1 = length(x);
n2 = length(y);

S = sqrt((((n1 - 1)*s1^2)+((n2-1)*s2^2))/(n1 + n2 - 2));

d = (u1 - u2) / S;

end

