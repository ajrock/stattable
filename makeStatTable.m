function [t, fig, uit] = makeStatTable(statVar,statSSS,varargin)
% (statVar,statSSS,tit, makeFig)
% statVar = vector of numerical data
% statSSS = vector of group ids (same length as statVar)
% tit = figure title string
% makeFig = flag for figure output
% HINT: If you made a boxplot or bar graph for thist data previously, use
% your data variable for the statVar and grouping var for statSSS
% makeFig = flag for figure output
% Varargin possibilities:
%   - flipSort: reverses the sorting order of group names
%   - starMode: Instead of giving P values, gives star significance levels
%   - rowAppend: add text to row names of table
%   - MakeFig: Make a figure
%   - title: title for table
%   - makeBoxPlot: wisotb


%2019-05-20 AC: added sig star mode, added flipSort

% Force row vectors
if size(statVar,1)==1
    statVar = statVar';
end
if size(statSSS,1)==1
    statSSS = statSSS';
end


% Check for lengths (aka if multiple data points per statSSS)
if size(statVar,2) ~= 1
    repL = size(statVar,2);
    statVar = reshape(statVar,numel(statVar),1);
    statSSS = repmat(statSSS,repL,1);
end

if any(contains(varargin,'makeBoxPlot'))
    figure('Position',[1113 553 280 420]); 
    boxplot(statVar,statSSS);
end


% Kruskalwallis
[p.KW,stats.KW]=anova1('kruskalwallis',statVar,statSSS,'off');
% Apparently 2012 vartestn did not have brown forsynthe. Using downloaded
% function requires refactoring information to numerical

% [~,I] = sort(statSSS);
% statVar = statVar(flipud(I));
% statSSS = statSSS(flipud(I));

[gidx,gnames,~] = grp2idx(statSSS);
if any(contains(varargin,'flipSort'))&& ~iscell(statSSS) % Table will display ordinal data with highest numbers at top
    gidx=grp2idx(-statSSS);
    gnames = flipud(gnames);
end

%p.BrownForsynthe = BFtest([statVar gidx]);
p.BrownForsythe = vartestn(statVar, statSSS, 'TestType','BrownForsythe','Display','off');

%[p.LeveneRobust,stats.LeveneRobust] = vartestn(statVar,statSSS,'off','BrownForsynthe');
[p.LeveneRobust,stats.LeveneRobust] = vartestn(statVar,statSSS,'TestType','LeveneAbsolute','Display','off');

% Try multicomparisons of MannWhitney U, Mood's test, and Welche's test
%if p.BrownForsynthe < 0.05 | p.LeveneRobust < 0.05 | p.KW < 0.05
    combos = combnk(1:length(gnames),2);
    for ii = 1:size(combos,1)
        group1 = statVar(find(gidx==combos(ii,1)));
        group2 = statVar(find(gidx==combos(ii,2)));
        %MWU
        try
            MWU(ii,1) = ranksum(group1,group2);
        catch
            MWU(ii,1) = NaN;
        end
        %moods
        %(http://www.real-statistics.com/non-parametric-tests/moods-median-test-two-samples/)
        groupMedian = median([group1; group2],'omitnan');
        x = [sum(group1>groupMedian),sum(group2>groupMedian);...
            sum(group1<groupMedian),sum(group2<groupMedian)];
        rowFacts = factorial(sum(x));
        colFacts = factorial(sum(x,2));
        sinFacts = factorial([x(:); sum(sum(x))]);
        dividend = rowFacts(1)*rowFacts(2)*colFacts(1)*colFacts(2);
        divisor = prod(sinFacts);
        %p.fish(ii) = dividend/divisor; Changed because numbers got 2 big
        fish(ii,1) = prod(rowFacts)/prod(sinFacts)*prod(colFacts);
        % Alternate moods with chi square
        rowSums = sum(x);
        colSums = sum(x,2);
        E = repmat(rowSums,2,1).*repmat(colSums,1,2)/sum(rowSums);
        chi = sum(sum(((x-E).^2)./E));
        mood(ii,1) = Chi2pval(chi,1); % Contingency table always gets 1 DOF
        % And finally contingency test for unequal variances
        try
            BFM(ii,1) = BFtest([group1, ones(length(group1),1); group2, 2*ones(length(group2),1)]);
        catch
             BFM(ii,1) = NaN;
        end
        group1name(ii,1) = gnames(combos(ii,1));
        group2name(ii,1) = gnames(combos(ii,2));
        % Effect size measures
        p.stats=mes(group1,group2,{'cles','rbcorr','U3'},...
            'isDep',0,...
            'missVal','listwise',...
            'nBoot',0);
        % Power calculation from MWU (cohen's d)
        d(ii,1) = myCohensD(group1,group2);
        statPow(ii,1) = myPowerCalc(d(ii,1),numel(group1),numel(group2),0.05);
        % Collect results
        %cles(ii,1) = p.stats.cles;
        %rbcorr(ii,1) = p.stats.rbcorr;
        med1(ii,1) = median(group1,'omitnan');
        med2(ii,1) = median(group2,'omitnan');
        mean1(ii,1) = mean(group1,'omitnan');
        mean2(ii,1) = mean(group2,'omitnan');
        std1(ii,1) = std(group1,'omitnan');
        std2(ii,1) = std(group2,'omitnan');
        iqr1(ii,1) = iqr(group1);
        iqr2(ii,1) = iqr(group2);
        U3(ii,1) = p.stats.U3;
        n1(ii,1) = numel(group1);
        n2(ii,1) = numel(group2);
        BFP(ii,1) = p.BrownForsythe;
        levene(ii,1) = p.LeveneRobust;
        KW(ii,1) = p.KW;
        
        % Bootstrap test too
        boot1 = bootstrp(2000,@(x) median(x, 'omitnan'),group1);
        boot2 = bootstrp(2000,@(x) median(x, 'omitnan'),group2);
        bootmed1(ii,1) = median(boot1,'omitnan');
        bootmed2(ii,1) = median(boot2,'omitnan');
        boottest(ii,1) = min([sum((boot1-boot2)<0)/2000; sum((boot2-boot1)<0)/2000]);

    end
    
    if any(contains(varargin,'starMode'))
       % Replaces numerical stats methods with stars. This is KW, BFP,
       % levene, MWU, mood, BFM, boottest
       kwStar = {};
       kwStar(KW>0.05)  = deal({'NS'});
       kwStar(KW<0.05)  = deal({'*'});
       kwStar(KW<0.01)  = deal({'**'});
       kwStar(KW<0.001) = deal({'***'});
       
       bfpStar = {};
       bfpStar(BFP>0.05)  = deal({'NS'});
       bfpStar(BFP<0.05)  = deal({'*'});
       bfpStar(BFP<0.01)  = deal({'**'});
       bfpStar(BFP<0.001) = deal({'***'});
       
       leveneStar = {};
       leveneStar(levene>0.05)  = deal({'NS'});
       leveneStar(levene<0.05)  = deal({'*'});
       leveneStar(levene<0.01)  = deal({'**'});
       leveneStar(levene<0.001) = deal({'***'});
       
       mwuStar = {};
       mwuStar(isnan(MWU))  = deal({'NaN'});
       mwuStar(MWU>0.05)  = deal({'NS'});
       mwuStar(MWU<0.05)  = deal({'*'});
       mwuStar(MWU<0.01)  = deal({'**'});
       mwuStar(MWU<0.001) = deal({'***'});
       
       moodStar = {};
       moodStar(isnan(mood))  = deal({'NaN'});
       moodStar(mood>0.05)  = deal({'NS'});
       moodStar(mood<0.05)  = deal({'*'});
       moodStar(mood<0.01)  = deal({'**'});
       moodStar(mood<0.001) = deal({'***'});
       
       bfmStar= {};
       bfmStar(isnan(BFM))  = deal({'NaN'});
       bfmStar(BFM>0.05)  = deal({'NS'});
       bfmStar(BFM<0.05)  = deal({'*'});
       bfmStar(BFM<0.01)  = deal({'**'});
       bfmStar(BFM<0.001) = deal({'***'});
       
       bootStar = {};
       bootStar(boottest>0.05)  = deal({'NS'});
       bootStar(boottest<0.05)  = deal({'*'});
       bootStar(boottest<0.01)  = deal({'**'});
       bootStar(boottest<0.001) = deal({'***'});
       
       KW = kwStar';
       BFP = bfpStar';
       levene = leveneStar';
       MWU = mwuStar';
       mood = moodStar';
       BFM = bfmStar';
       boottest = bootStar';
       
    end
    
    %Form table
    if any(contains(varargin,'rowAppend'))
        rowAppend = varargin{find(contains(varargin,'rowAppend'))+1};
    else
        rowAppend = '';
    end
    t=table(KW,BFP,levene,MWU,mood,BFM,boottest,d,U3,statPow,n1,n2,bootmed1,bootmed2,med1,med2,iqr1,iqr2,mean1,mean2,std1,std2,'RowNames',strcat(group1name, {' vs '}, group2name, rowAppend));
    if any(contains(varargin,'MakeFig'))
        fig=figure('Position',[40 25 1000 250],'Name',varargin{find(contains(varargin,'title'))+1});
        uit = uitable('Data',t{:,:},'ColumnName',t.Properties.VariableNames,...
            'RowName',t.Properties.RowNames,'Units', 'Normalized', 'Position',[0 0 1 1]);%,...
            %'ColumnWidth',{50 50 50 50 50 50 50 50 50 50 50 50 50 50 50 50 50 50});
    end
%else
%    t = [];
%end

end


function mood = moodsTest(group1, group2)
% Perform moods test, which checks range of values for two groups against
% the group median using a Chi contingency table
% INPUT: two vectors of numerical data for comparison

groupMedians = median([group1; group2]);

% Form contingency table
xVal = [sum(group1>groupMedians),sum(group2>groupMedians);...
    sum(group1<groupMedians),sum(group2<groupMedians)];

rowSum = sum(xVal);
colSum = sum(xVal,2);

% Look up Chi distribution for explanation of what the expected value is.
expectedValue = repmat(rowSum,2,1).*repmat(colSum,1,2)/sum(rowSum);
chiVal = sum(sum(((xVal-expectedValue).^2)./expectedValue));

% need this function from stats toolbox. If missing, ask.
mood = Chi2pval(chiVal,1); % DOF always = 1 for contingency tables

end
